# Frequent Itemset Mining
---

Identifying frequently rated movies by implementing of SON algorithm using Apache Spark.


# Commands
---

Python implementation Solution folder contains python code Kalpesh_Adhatrao_SON.py for SON implementation. It is to be executed as follows,


```
spark-submit Kalpesh_Adhatrao_SON.py <case_number> <rating_file_path> <support_value>
```

Arguments:

	1. case_number: Indicates the case number (1 or 2) which is to be executed 
	2. rating_file_path: Indicates the ratings.csv file path 
	3. sport_value: Indicates support threashold value

Scala implementation Solution folder contains Kalpesh_Adhatrao_SON.jar containing Scala code for SON implementation. It is to be executed as follows,


```
spark-submit --class FrequentItemsetsSON Kalpesh_Adhatrao_SON.jar <case_number> <rating_file_path> <support_value>
```

Arguments: 

	1. case_number: Indicates the case number (1 or 2) which is to be executed 
	2. rating_file_path: Indicates the ratings.csv file path 
	3. sport_value: Indicates support threashold value

**Note:** The output file will be generated in same directory from which above commands are executed
