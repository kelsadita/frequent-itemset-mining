import sys
import time
from collections import defaultdict

from pyspark import SparkConf, SparkContext

# --------------------------------------------------------------------
# Constants
# --------------------------------------------------------------------


# --------------------------------------------------------------------
# Apriori algorithm implementation and its utility functions
# --------------------------------------------------------------------

def join(item_set, candidate_pair_size):
    candidate_pair = set()
    for part1 in item_set:
        for part2 in item_set:
            parts_union = part1.union(part2)
            if len(parts_union) == candidate_pair_size:
                candidate_pair.add(parts_union)
    return candidate_pair


def get_item_set_count_in_basket(item_set, basket_iterator):
    return sum([1 for basket in basket_iterator if basket[1].issuperset(item_set)])


def apriori_partition(basket_iterator, total_baskets):
    # Pass 1: calculate singletons
    singleton_count = {}
    bucket_counts = 0
    baskets = list(basket_iterator)
    for basket in baskets:
        bucket_counts += 1
        for item in basket[1]:
            singleton_item = frozenset([item])
            if singleton_item not in singleton_count:
                singleton_count[singleton_item] = 0
            singleton_count[singleton_item] += 1

    chunk_support = (float(bucket_counts) / float(total_baskets)) * support
    freq_singletons = [key for key, value in singleton_count.iteritems() if value >= chunk_support]

    # Pass 2: Calculate doubles, triples and so on
    combination_size = 2
    current_freq_set = freq_singletons
    freq_sets = dict()
    while current_freq_set != set([]):
        freq_sets[combination_size - 1] = current_freq_set
        current_freq_set = join(current_freq_set, combination_size)

        next_freq_set = set([])
        for item_set in current_freq_set:
            item_set_count = get_item_set_count_in_basket(item_set, baskets)
            if item_set_count >= chunk_support:
                next_freq_set.add(item_set)
        current_freq_set = next_freq_set
        combination_size += 1

    final_freq_itemsets = []
    for item_set in freq_sets.values():
        for freq_item in item_set:
            final_freq_itemsets.append((freq_item, 1))

    yield final_freq_itemsets


# --------------------------------------------------------------------
# Map reduce tasks specific to SON implementation
# --------------------------------------------------------------------

def map_task1():
    return baskets_rdd \
        .mapPartitions(lambda partition: apriori_partition(partition, num_baskets))


def reduce_task1(freq_items_rdd):
    return freq_items_rdd.flatMap(list).reduceByKey(lambda a, b: a)


def freq_items_counter(basket_iterator):
    freq_item_set_and_count = dict()
    baskets = list(basket_iterator)
    for frequent_item in freq_items_reduce_1:
        freq_item_set = frequent_item[0]
        freq_item_set_and_count[freq_item_set] = 0
        for basket in baskets:
            if basket[1].issuperset(freq_item_set):
                freq_item_set_and_count[freq_item_set] += 1

    yield freq_item_set_and_count.items()


def map_task2():
    return baskets_rdd.mapPartitions(freq_items_counter)


def reduce_task2(freq_items):
    return freq_items.flatMap(list) \
        .reduceByKey(lambda value1, value2: value1 + value2) \
        .filter(lambda itemset_and_support: itemset_and_support[1] >= support)


# --------------------------------------------------------------------
# Utility functions required for combiner
# --------------------------------------------------------------------

def merge_value(x, y):
    x.add(y)
    return x


def generate_output_file_name():
    prefix = "Kalpesh_Adhatrao_SON_"
    rating_file_path_parts = rating_file_path.split("/")
    rating_file_name = rating_file_path_parts[len(rating_file_path_parts) - 1].replace(".csv", "")
    return prefix + rating_file_name + ".case" + str(case_number) + "-" + str(int(support)) + ".txt"


def save_output():
    final_freq_items = defaultdict(list)
    for freq_item_and_count in freq_items_reduce2:
        freq_item = freq_item_and_count[0]
        freq_item_len = len(freq_item)
        final_freq_items[freq_item_len].append(sorted(freq_item))

    all_item_sets_list = []
    for key in sorted(final_freq_items):
        sorted_list = sorted(map(list, final_freq_items[key]))
        item_sets_strings = []
        for item_list in sorted_list:
            item_sets_strings.append("(" + ", ".join(map(str, item_list)) + ")")
        all_item_sets_list.append(", ".join(item_sets_strings))

    final_output = "\n\n".join(all_item_sets_list)

    with open(generate_output_file_name(), "w") as output_file:
        output_file.write(final_output)


if __name__ == "__main__":
    start_time = time.time()

    case_number = int(sys.argv[1])
    rating_file_path = sys.argv[2]
    support = int(sys.argv[3])

    sc = SparkContext(appName='Movies and users frequent items', conf=SparkConf())

    # Setting log level to WARN
    sc.setLogLevel("WARN")

    raw_input_rdd = sc.textFile(rating_file_path)
    header = raw_input_rdd.first()
    raw_input_rdd = raw_input_rdd.filter(lambda line: line != header).map(lambda line: line.split(","))

    if case_number == 1:
        input_rdd = raw_input_rdd.map(lambda entry: (int(entry[0]), int(entry[1])))
    else:
        input_rdd = raw_input_rdd.map(lambda entry: (int(entry[1]), int(entry[0])))

    # Preparing baskets based on requirements
    baskets_rdd = input_rdd.combineByKey(
        lambda entry: {entry}, lambda x, y: merge_value(x, y), lambda x, y: x.union(y)).repartition(1).cache()

    num_baskets = baskets_rdd.count()

    # Calling SON specific functions
    freq_items_map1 = map_task1()
    freq_items_reduce_1 = reduce_task1(freq_items_map1).collect()

    freq_items_map2 = map_task2()
    freq_items_reduce2 = reduce_task2(freq_items_map2).collect()

    save_output()

    end_time = time.time()
    print "Total time taken:", (end_time - start_time), " seconds"
