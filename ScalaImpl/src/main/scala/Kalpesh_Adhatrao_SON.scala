import java.io.{File, PrintWriter}

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

object FrequentItemsetsSON {

  def sort[T: Ordering](coll: Seq[Iterable[T]]): Seq[Iterable[T]] = coll.sorted

  // --------------------------------------------------
  // Apriori algorithm implementation specific methods
  // --------------------------------------------------

  def join(currentFrequentSet: Set[Set[Int]], combinationSize: Int): Set[Set[Int]] = {
    var candidatePair: Set[Set[Int]] = Set.empty

    currentFrequentSet.foreach(part1 => {
      currentFrequentSet.foreach(part2 => {
        var partUnion = part1.union(part2)
        if (partUnion.size == combinationSize) {
          candidatePair += partUnion
        }
      })
    })

    candidatePair
  }

  def getItemSetCountInBuckets(itemSet: Set[Int], baskets: List[(Int, Set[Int])]): Int = {
    baskets.count(basket => itemSet.subsetOf(basket._2))
  }

  def aprioriPartion(partition: Iterator[(Int, Set[Int])], totalBaskets:Int, support: Int): Iterator[(Set[Int], Int)] = {
    // Pass1: Calculate singleton
    val singletonCountMap = new mutable.HashMap[Set[Int], Int]()

    val baskets: List[(Int, Set[Int])] = partition.toList

    baskets.foreach(basket => {
      val basketItems = basket._2
      basketItems.foreach(item => {
        val singletonItem = Set(item)
        val newCount: Int = singletonCountMap.getOrElse(singletonItem, 0) + 1
        singletonCountMap.put(singletonItem, newCount)
      })
    })

    val chunkSupport = (baskets.size.toFloat / totalBaskets.toFloat) * support
    val freqSingletons = singletonCountMap.filter(itemSetAndCount => itemSetAndCount._2 >= chunkSupport).keySet.toSet

    // Pass 2: Calculate double, triples and so on
    var combinationSize = 2
    var currentFrequentSet = freqSingletons
    val freqItemSetMap: mutable.HashMap[Int, Set[Set[Int]]] = new mutable.HashMap[Int, Set[Set[Int]]]()
    while (currentFrequentSet.nonEmpty) {
      freqItemSetMap.put(combinationSize - 1, currentFrequentSet)
      currentFrequentSet = join(currentFrequentSet, combinationSize)

      var nextFreqSet: Set[Set[Int]] = Set.empty
      currentFrequentSet.foreach(itemSet => {
        val itemSupport: Int = getItemSetCountInBuckets(itemSet, baskets)
        if(itemSupport >= chunkSupport) {
          nextFreqSet += itemSet
        }
      })
      currentFrequentSet = nextFreqSet
      combinationSize += 1
    }

    var allSizeFreqItems: Set[(Set[Int], Int)] = Set.empty
    freqItemSetMap.values.foreach(freqItemSetOfASize => {
      freqItemSetOfASize.foreach(freqItemSet => {
        allSizeFreqItems += ((freqItemSet, 1))
      })
    })

    allSizeFreqItems.iterator
  }

  // --------------------------------------------------
  // SON Algorithm implementation specific map reduce functions
  // --------------------------------------------------

  def mapTask1(basketRdd: RDD[(Int, Set[Int])], totalBaskets: Int, support: Int): RDD[(Set[Int], Int)] = {
    basketRdd.mapPartitions(partition => aprioriPartion(partition, totalBaskets, support))
  }

  def reduceTask1(freqItemsRdd: RDD[(Set[Int], Int)]): RDD[(Set[Int], Int)] =
    freqItemsRdd.reduceByKey((key1, key2) => key1)

  def frequentItemCounter(partition: Iterator[(Int, Set[Int])], freqItemsReduce1: Array[(Set[Int], Int)]): Iterator[(Set[Int], Int)] = {
    val freqItemsAndCount: mutable.HashMap[Set[Int], Int] = new mutable.HashMap[Set[Int], Int]()
    val baskets: List[(Int, Set[Int])] = partition.toList

    freqItemsReduce1.foreach(freqItemsMap => {
      val freqItemSet: Set[Int] = freqItemsMap._1
      freqItemsAndCount.put(freqItemSet, 0)
      baskets.foreach(basket => {
        if(freqItemSet.subsetOf(basket._2)) {
          freqItemsAndCount.put(freqItemSet, freqItemsAndCount.getOrElse(freqItemSet, 0) + 1)
        }
      })
    })

    freqItemsAndCount.iterator
  }

  def mapTask2(basketRdd: RDD[(Int, Set[Int])], freqItemsReduce1: Array[(Set[Int], Int)]): RDD[(Set[Int], Int)] =
    basketRdd.mapPartitions(partition => frequentItemCounter(partition, freqItemsReduce1))

  def reduceTask2(freqItemsFromMap2: RDD[(Set[Int], Int)], support: Int): RDD[(Set[Int], Int)] = {
    freqItemsFromMap2.reduceByKey(_ + _).filter(itemSetAndSupport => itemSetAndSupport._2 >= support)
  }

  // --------------------------------------------------
  // Utility functions required for the combiner
  // --------------------------------------------------
  def intToSet(x: Int): Set[Int] = Set(x)

  def addIntToSet(acc: Set[Int], x: Int): Set[Int] = acc.union(Set(x))

  def unionTwoIntSet(acc1: Set[Int], acc2: Set[Int]): Set[Int] = acc1.union(acc2)

  // --------------------------------------------------
  // Utility for saving final output
  // --------------------------------------------------

  def saveOutput(freqItemsReduce2: Array[(Set[Int], Int)], outputFilePath: String): Unit = {
    val freqItemsCountAndItems = new mutable.HashMap[Int, List[Seq[Int]]]()

    freqItemsReduce2.foreach(freqItemsAndCount => {
      val freqItem: Seq[Int] = freqItemsAndCount._1.toSeq.sorted
      val freqItemLength = freqItem.size
      val newFreqItemsOfGivenSize =
        freqItem :: freqItemsCountAndItems.getOrElse(freqItemLength, List[Seq[Int]]())
      freqItemsCountAndItems.put(freqItemLength, newFreqItemsOfGivenSize)
    })

    var allItemSetsList: List[String] = List.empty
    val sortedFreqCountAndItems = freqItemsCountAndItems.toSeq.sortBy(_._1).reverse
    sortedFreqCountAndItems.foreach(freqItemsCountAndItem => {
      var itemSetsString: List[String] = List.empty
      sort(freqItemsCountAndItem._2).foreach(itemList => {
        itemSetsString =  itemSetsString :+ "(" + itemList.map(_.toString).mkString(", ") + ")"
      })
      allItemSetsList = itemSetsString.mkString(", ") :: allItemSetsList
    })

    val finalOutput = allItemSetsList.mkString("\n\n")
    val pw = new PrintWriter(new File(outputFilePath))
    pw.write(finalOutput)
    pw.close()
  }

  def getOutputFileName(ratingFilePath: String, caseNumber: Int, support: Int): String = {
    val prefix: String = "Kalpesh_Adhatrao_SON_"
    val ratingFilePathPart: Array[String] = ratingFilePath.split("/")
    val ratingFileName = ratingFilePathPart(ratingFilePathPart.size - 1).replace(".csv", "")
    prefix + ratingFileName + ".case" + caseNumber.toString + "-" + support.toString + ".txt"
  }

  def main(args: Array[String]): Unit = {
    val scriptStartTime = System.nanoTime

    val caseNumber = args(0).toInt
    val ratingFile = args(1)
    val support = args(2).toInt

    val sparkConf  = new SparkConf()
      .setAppName("Movies and users frequent items")
      .set("spark.driver.memory", "5G")

    val sc = new SparkContext(sparkConf)

    // Setting log level to WARN
    sc.setLogLevel("WARN")

    val rawInputRdd = sc.textFile(ratingFile)
    val header = rawInputRdd.first()
    val rawInputRddArray = rawInputRdd.filter(line => !line.equals(header)).map(line => line.split(","))

    var preProcessedInputRdd: RDD[(Int, Int)] = null
    if(caseNumber == 1) {
      preProcessedInputRdd = rawInputRddArray.map(entry => (entry(0).toInt, entry(1).toInt))
    } else {
      preProcessedInputRdd = rawInputRddArray.map(entry => (entry(1).toInt, entry(0).toInt))
    }

    var basketRdd =
      preProcessedInputRdd.combineByKey(intToSet, addIntToSet, unionTwoIntSet)

    val numBaskets: Int = basketRdd.count().toInt

    // Repartitioning the RDD
    if ((support / 8) < 2) {
      println("Processing data with 1 partition (very small data)")
      basketRdd = basketRdd.coalesce(1).cache()
    } else {
      println("Processing data with default partitioning: " + basketRdd.getNumPartitions)
    }

    // Map reduce #1
    val freqItemsFromMap1 = mapTask1(basketRdd, numBaskets, support)
    val freqItemsReduce1 = reduceTask1(freqItemsFromMap1).collect()

    // Map reduce #2
    val freqItemsFromMap2 = mapTask2(basketRdd, freqItemsReduce1)
    val freqItemsReduce2 = reduceTask2(freqItemsFromMap2, support).collect()

    saveOutput(freqItemsReduce2, getOutputFileName(ratingFile, caseNumber, support))

    val duration = (System.nanoTime - scriptStartTime) / 1e9d
    println("Total time taken: " + duration + " seconds")
  }
}