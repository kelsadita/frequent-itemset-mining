name := "ScalaHw2"

version := "0.1"

scalaVersion := "2.11.0"

val sparkVersion = "2.2.1"

resolvers ++= Seq(
  "apache-snapshots" at "http://repository.apache.org/snapshots/"
)

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion