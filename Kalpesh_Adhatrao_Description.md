# Homework 2

### Approach followed to solve given prolem statements
---

**SON Algorithm**

Both scala and python code are properly commented to decribe the flow of the implementation. Following is a brief description regarding the implementation
- RDD is created from the input CSV file and applied `combineByKey` operation on it to generate an RDD represeting the baskets based on given cases (1 or 2)
  - Format for **case1**: (movie11, movie12, movie13,...)
  - Format for **case2**: (user11, user12, user13,...)
- The implementation is then divided into 2 map reduce tasks as follows
- **Map Task 1:**
  - Using `mapPartitions` function apply `apriori algorithm` on given partition of data to generate frequent items
  - The support used for generating these items is `(number of baskets in chunk / total baskets) * support` 
  - This map task emits `(candidate frequent item set, 1)` for each canidate frequent item set.
- **Reduce Task 1:**
  - The reduce task just ignores values from map task. For a frequent item it will emit that frequent item if it is appeared one or more than one time. The output of this first reduce task is just set of candidate itemsets.
  - Here, `reduceByKey` function is used to perform given task
- **Map Task 2:**
  - This stage checks the occurences of calculated candidate frequent items in the assigned partition of basket. Once again `mapPartitions` function is used to accomplish given task
  - It emits `(candidate frequent item set, number of occurence of frequent item in the basket partition)` for each frequent item set
- **Reduce Task 2:**
  - This stage combines all the occurrences of given candidate frequent item sets produced from map tasks. If this count is greater than support than the candidate item set is truely a frequent item set. Here, `reduceByKey` and `filter` functions are used on RDD to perform given tasks
  - It emits `(frequent item set, count)`
- Finally all the frequent items collected and are written to output file based on given specification

<br>
<br>
### Spark and Scala versions used for implementation
---

**Spark Version:** 2.2.1
**SBT Version:** 1.1.5
**Scala Version:** 2.11.0
**Python Version:** 2.7.13

### 

<br>
<br>
### Commands to execute solution file
---

**Python implementation**
Solution folder contains python code `Kalpesh_Adhatrao_SON.py` for SON implementation. It is to be executed as follows,
```bash
spark-submit Kalpesh_Adhatrao_SON.py <case_number> <rating_file_path> <support_value>
```
**Arguments:**
1. **case_number:** Indicates the case number (1 or 2) which is to be executed
1. **rating_file_path:** Indicates the ratings.csv file path
1. **sport_value:** Indicates support threashold value

**Scala implementation**
Solution folder contains `Kalpesh_Adhatrao_SON.jar` containing Scala code for SON implementation. It is to be executed as follows,
```bash
spark-submit --class FrequentItemsetsSON Kalpesh_Adhatrao_SON.jar <case_number> <rating_file_path> <support_value>
```

**Arguments:**
1. **case_number:** Indicates the case number (1 or 2) which is to be executed
1. **rating_file_path:** Indicates the ratings.csv file path
1. **sport_value:** Indicates support threashold value

**Note:** The output file will be generated in same directory from which above commands are executed

<br>
<br>
<br>
<br>
### 
### Execution times for various cases
---

![Image](/Users/kalpesh/Dropbox/Documents/medley/resources/Hkr2ZC_lQ_BkFolYsxQ.png)
